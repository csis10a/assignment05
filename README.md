# GridWorld Case Study

The assignment tasks are located [here](https://docs.google.com/document/d/1aYyqDauwgDfpxpQvguQE6VazQkq7bMFxOBJbJkrUpTk/edit?usp=sharing)

JavaDocs for GridWorld can be found at [http://www.greenteapress.com/thinkapjava/javadoc/gridworld/](http://www.greenteapress.com/thinkapjava/javadoc/gridworld/)

The source code for GridWorld is also available at [http://hohonuuli.bitbucket.org/csis10a/ch05/GridWorldCode.zip](http://hohonuuli.bitbucket.org/csis10a/ch05/GridWorldCode.zip)

## Getting Started

1. Open a terminal, command prompt or PowerShell and change to your working directory (e.g. U:)
2. Grab a copy of the starter project using:  
`git clone https://bitbucket.org/csis10a/assignment05.git`
3. If you have maven installed you can run the project:
- Windows: `run.bat`
- Everything else: `run.sh`
4. This assignment is configured as a maven project. You can open it directly in most IDEs. If you're using BlueJ follow the instructions in the  [installation guide](http://www.collegeboard.com/prod_downloads/student/testing/ap/compsci_a/ap07_gridworld_installation_guide.pdf). 